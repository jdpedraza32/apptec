package com.sigmotoa.deif.medicamento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.deif.medicamento.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var primera:Button=findViewById(R.id.button)
        var segunda:Button=findViewById(R.id.button2)
        var tercera:Button=findViewById(R.id.button3)
        var cuarta:Button=findViewById(R.id.button000)
        primera.setOnClickListener { actor1() }
        segunda.setOnClickListener { actor2() }
        tercera.setOnClickListener { actor3() }
        cuarta.setOnClickListener { actor4() }

    }
    fun actor1(){
        val intent = Intent(this, info::class.java)
        startActivity(intent)
    }
    fun actor2(){
        val intent = Intent(this, diario::class.java)
        startActivity(intent)
    }
    fun actor3(){
        val intent = Intent(this, fisica::class.java)
        startActivity(intent)
    }

    fun actor4(){
        val intent = Intent(this, AboutUs::class.java)
        startActivity(intent)
    }
}